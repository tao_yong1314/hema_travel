package cn.itcast.travel.dao;

import cn.itcast.travel.domain.User;

public interface UserDao {

    /**
     * 根据姓名查询用户信息
     * @param username
     * @return
     */
    public User findByUsername(String username);

    /**
     * 保存用户
     * @param user
     */
    public void save(User user);

    /**
     * 根据code查询用户
     * @param code
     * @return
     */
    User findByCode(String code);

    /**
     * 修改激活状态
     * @param user
     */
    void updateStatus(User user);

    /**
     * 根据用户名、密码来查询用户
     * @return
     */
    User findByUsernameAndPassword(String username,String password);
}
