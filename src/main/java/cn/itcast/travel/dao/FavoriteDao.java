package cn.itcast.travel.dao;

import cn.itcast.travel.domain.Favorite;

public interface FavoriteDao {

    /**
     * 查询收藏信息
     * @param rid
     * @param uid
     * @return
     */
    public Favorite isFavorite(int rid, int uid);

    /**
     * 根据rid查询次数
     * @param rid
     * @return
     */
    public int findCountByRid(int rid);

    /**
     * 添加收藏
     * @param rid
     * @param uid
     */
    void addFavorite(String rid, int uid);
}
