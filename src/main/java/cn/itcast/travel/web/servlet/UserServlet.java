package cn.itcast.travel.web.servlet;

import cn.itcast.travel.domain.ResultInfo;
import cn.itcast.travel.domain.User;
import cn.itcast.travel.service.UserService;
import cn.itcast.travel.service.impl.UserServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.beanutils.BeanUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * active
 * exit
 * findUser
 * login
 * register
 */
@WebServlet("/userServlet/*")
public class UserServlet extends BaseServlet {
    public void active(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
        //1.获取激活码  http://localhost/activeUserServlet?code=94fca01e471d41399572feb42595a212
        String code = req.getParameter("code");
        System.out.println(code);
        if (code != null){
            //2.调用service完成激活
            UserServiceImpl service = new UserServiceImpl();
            boolean flag = service.active(code);
            System.out.println(flag);

            //3.判断标记
            String msg = null;
            if (flag){
                //激活成功
                msg = "激活成功，请<a href='http://localhost/login.html'>登录</a>";
            }else{
                //激活失败
                msg = "激活失败，请联系管理员";
            }
            resp.setContentType("text/html;charset=utf-8");
            resp.getWriter().write(msg);
        }
    }

    public void exit(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
        req.getSession().invalidate();
        resp.sendRedirect(req.getContextPath()+"/login.html");
    }

    public void findUser(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
        Object user = req.getSession().getAttribute("user");
        System.out.println(user);
        //将info对象序列化为json
//        ObjectMapper mapper = new ObjectMapper();
//        //将json数据写回客户端
//        //设置content-type
//        resp.setContentType("application/json;charset=utf-8");
//        mapper.writeValue(resp.getOutputStream(),user);
        writeValue(user,resp);
    }

    public void login(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
        System.out.println("进入loginServlet");
        HttpSession session = req.getSession();
        String checkcode_server = (String) session.getAttribute("CHECKCODE_SERVER");
        System.out.println(checkcode_server);
        String check = req.getParameter("check");
        System.out.println(check);
        session.removeAttribute("CHECKCODE_SERVER");

        //比较
        if (checkcode_server== null || !checkcode_server.equalsIgnoreCase(check)){
            ResultInfo info = new ResultInfo();
            //验证码错误
            info.setFlag(false);
            info.setErrorMsg("验证码错误！");
            //将info对象序列化为json
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(info);
            System.out.println(json);
            resp.setContentType("application/json;charset=utf-8");
            resp.getWriter().write(json);
            return;
        }

        //1.获取对象
        Map<String, String[]> map = req.getParameterMap();
        //2.封装对象
        User user = new User();
        try {
            BeanUtils.populate(user,map);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //3.调用service完成注册
        UserService service = new UserServiceImpl();
        User u = service.login(user);
        ResultInfo info = new ResultInfo();
        System.out.println("登录结果："+u);
        //4.用户名或密码错误
        if (u==null){
            info.setFlag(false);
            info.setErrorMsg("用户名或密码错误");
            System.out.println("账号密码错误");
        }
        //5.判断用户是否激活
        if(u != null && u.getStatus().equals("N")){
            //用户尚未激活
            info.setFlag(false);
            info.setErrorMsg("您尚未激活");
            System.out.println("未激活");
        }
        //6.判读登录成功
        if (u !=null && u.getStatus().equals("Y")){
//            req.getSession().setAttribute("uid",u.getUid());
            req.getSession().setAttribute("user",u);
            info.setFlag(true);
        }

        //将info对象序列化为json
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(info);

        //将json数据写回客户端
        //设置content-type
        resp.setContentType("application/json;charset=utf-8");
        resp.getWriter().write(json);
    }

    public void register(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
        // 验证校验
        String check = req.getParameter("check");
        System.out.println(check);
        HttpSession session = req.getSession();
        String checkcode_server = (String) session.getAttribute("CHECKCODE_SERVER");
        System.out.println(checkcode_server);
        session.removeAttribute("CHECKCODE_SERVER");
        //比较
        if (checkcode_server== null || !checkcode_server.equalsIgnoreCase(check)){
            ResultInfo info = new ResultInfo();
            //验证码错误
            info.setFlag(false);
            info.setErrorMsg("验证码错误！");
            //将info对象序列化为json
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(info);
            System.out.println(json);
            resp.setContentType("application/json;charset=utf-8");
            resp.getWriter().write(json);
            return;
        }

        //1.获取对象
        Map<String, String[]> map = req.getParameterMap();
        //2.封装对象
        User user = new User();
        try {
            BeanUtils.populate(user,map);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //3.调用service完成注册
        UserService service = new UserServiceImpl();
        boolean flag = service.register(user);
        ResultInfo info = new ResultInfo();
        System.out.println("注册结果："+flag);
        //4.响应结果
        if (flag){
            info.setFlag(true);
        }else{
            info.setFlag(false);
            info.setErrorMsg("注册失败");
        }

        //将info对象序列化为json
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(info);

        //将json数据写回客户端
        //设置content-type
        resp.setContentType("application/json;charset=utf-8");
        resp.getWriter().write(json);
    }


}
