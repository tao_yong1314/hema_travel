package cn.itcast.travel.web.servlet;

import cn.itcast.travel.domain.PageBean;
import cn.itcast.travel.domain.Route;
import cn.itcast.travel.domain.User;
import cn.itcast.travel.service.FavoriteService;
import cn.itcast.travel.service.RouteService;
import cn.itcast.travel.service.impl.FavoriteServiceImpl;
import cn.itcast.travel.service.impl.RouteServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/route/*")
public class RouteServlet extends BaseServlet {
    private RouteService service = new RouteServiceImpl();
    private FavoriteService favoriteService= new FavoriteServiceImpl();

    /**
     * 分页查询
     *
     * @param req
     * @param resp
     */
    public void pageQuery(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        //1.接收参数
        String currentPageStr = req.getParameter("currentPage");
        String pageSizeStr = req.getParameter("pageSize");
        String cidStr = req.getParameter("cid");
        String rname = req.getParameter("rname");

        //2.处理参数
        int cid = 0;
        if (!cidStr.equals("null") && cidStr != null && cidStr.length() > 0) {
            cid = Integer.parseInt(cidStr);
        }
        int currentPage = 0;
        if (currentPageStr != null && currentPageStr.length() > 0) {
            currentPage = Integer.parseInt(currentPageStr);
        } else {
            currentPage = 1;
        }

        int pageSize = 0;
        if (pageSizeStr != null && pageSizeStr.length() > 0) {
            pageSize = Integer.parseInt(pageSizeStr);
        } else {
            pageSize = 5;
        }

        //3.调用service查询
        PageBean<Route> routePageBean = service.pageQuery(cid, currentPage, pageSize, rname);
        writeValue(routePageBean, resp);
    }

    /**
     * 根据id查询一个旅游线路的详细信息
     *
     * @param req
     * @param resp
     * @throws IOException
     */
    public void findOne(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String rid = req.getParameter("rid");
        Route route = service.findOne(rid);
        writeValue(route, resp);
    }

    /**
     * 判读用户是否收藏过线路
     * @param req
     * @param resp
     * @throws IOException
     */
    public void isFavorite(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String rid = req.getParameter("rid");
        User user = (User) req.getSession().getAttribute("user");
        int uid;
        uid = user==null ? 0 : user.getUid();
        boolean flag = favoriteService.isFavorite(rid, uid);
        writeValue(flag,resp);
    }

    public void addFavorite(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String rid = req.getParameter("rid");
        User user = (User) req.getSession().getAttribute("user");
        int uid;
        uid = user==null ? 0 : user.getUid();
        favoriteService.addFavorite(rid, uid);
    }

    }
