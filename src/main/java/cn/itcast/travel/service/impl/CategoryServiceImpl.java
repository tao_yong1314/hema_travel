package cn.itcast.travel.service.impl;

import cn.itcast.travel.dao.CategoryDao;
import cn.itcast.travel.dao.impl.CategoryDaoImpl;
import cn.itcast.travel.domain.Category;
import cn.itcast.travel.service.CategoryService;
import cn.itcast.travel.util.JedisUtil;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Tuple;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class CategoryServiceImpl implements CategoryService {

    private CategoryDao categoryDao = new CategoryDaoImpl();


    /*
    查询所有用户
     */
    @Override
    public List<Category> findAll() {
//        1.从redis中查询
//        1.1获取jdedis客户端
        Jedis jedis = JedisUtil.getJedis();
        //1.2可使用sortedset排序
        //Set<String> categorys = jedis.zrange("category", 0, -1);
//        1.3查询sortedset中的分数(cid)和值(cname)
        Set<Tuple> categorys = jedis.zrangeWithScores("category", 0, -1);
        System.out.println("jdeis连接成功....");
        //2.判断集合是否为空
        List<Category> list = null;
        if (categorys==null || categorys.size()==0){
            System.out.println("从数据库查询....");
            //从数据库查询
            list = categoryDao.findAll();
            for (int i = 0; i < list.size(); i++) {
                jedis.zadd("category",list.get(i).getCid(),list.get(i).getCname());
            }
        }else{
            System.out.println("从redis中查询....");
            //如何不为空，将set的数据存入list
            list = new ArrayList<Category>();
            for (Tuple s : categorys) {
                Category category = new Category();
                category.setCname(s.getElement());
                category.setCid((int)s.getScore());
                list.add(category);
            }
        }

        return list;
//        return  categoryDao.findAll();
    }
}
