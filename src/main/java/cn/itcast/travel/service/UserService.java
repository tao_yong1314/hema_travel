package cn.itcast.travel.service;

import cn.itcast.travel.domain.User;

public interface UserService {
    /**
     * 注册方法
     * @param user
     * @return
     */
    boolean register(User user);

    /**
     * 激活方法
     * @param code
     * @return
     */
    boolean active(String code);

    User login(User user);
}
