package cn.itcast.travel.service;

import cn.itcast.travel.domain.PageBean;
import cn.itcast.travel.domain.Route;

public interface RouteService {

    /**
     * 线路查询
     * @param cid
     * @param currentPage
     * @param pageSize
     * @param rname
     * @return
     */

    public PageBean<Route> pageQuery(int cid,int currentPage,int pageSize,String rname);

    /**
     * 根据cid查询
     * @param rid
     * @return
     */
    public Route findOne(String rid);

}
